module.exports = function (app) {

  var express = require('express')
  var jwt = require('express-jwt')({secret: app.secret})
  var Orders = rootRequire('controllers/c_orders')(app)
  var Base = rootRequire('controllers/c_bases')(app)
  var Estil = rootRequire('controllers/c_estils')(app)
  var User = rootRequire('controllers/c_user')(app)
  var util = rootRequire('util')
  var router = express.Router()

  router.use(jwt)

  router.get('/api/users/profile',User.profile,util.sendAuthError)
  router.get('/api/users/logout',User.logout,util.sendAuthError)
  router.get('/api/orders/:id', Orders.getById)
  router.get('/api/users/self/orders', Orders.getOrders, util.sendAuthError)
  router.post('/api/users/self/orders', Orders.create, util.sendAuthError)
  router.post('/api/base/create/',Base.create, util.sendAuthError)
  router.post('/api/estil/create',Estil.create,util.sendAuthError)
  router.delete('/api/base/delete',Base.delete,util.sendAuthError)
  router.post('/api/base/associarEstil',Base.associarEstil,util.sendAuthError)
  router.delete('/api/base/desassociarEstil',Base.desassociarEstil,util.sendAuthError)
  router.delete('/api/estil/delete',Estil.delete,util.sendAuthError)
  return router;
}