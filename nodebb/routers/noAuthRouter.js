module.exports = function (app) {

  var express = require('express')
  var User = rootRequire('controllers/c_user')(app)
  var Base = rootRequire('controllers/c_bases')(app)
  var Estil = rootRequire('controllers/c_estils')(app)
  var util = rootRequire('util')
  var router = express.Router()



  router.get('/api/base/showAllBases',Base.getBases)
  router.post('/api/base/showAllBases',Base.getBases)
  // User login
  router.post('/api/users/login', User.login)
  //Check logged in
  router.get('/api/users/check', User.check)
  // User registration
  router.post('/api/users', User.create)
  router.get('/api/estil/mostraEstils', Estil.mostraEstils)
  router.get('/api/base/estils/:baseid',Base.mostraEstilsBsae)
  router.get('/api/user/bases/:username',User.mostrarUserBases)

  return router;
}