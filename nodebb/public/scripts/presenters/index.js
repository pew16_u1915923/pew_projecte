define([
  'presenters/ui',
  'presenters/routes',
  'presenters/login',
  'presenters/base',
  'presenters/estil',
  'presenters/perfilui'],
  function(Ui, Router, Login,Base,Estil,PerfilUi) {

  var Presenters = {}

  Presenters.Ui = Ui
  Presenters.PerfilUi = PerfilUi 
  Presenters.Router = Router
  Presenters.Base = Base
  Presenters.Estil = Estil

  Presenters.init = function() {
    Ui.init()
    PerfilUi.init()
    Login.init(Presenters)
    Router.init(Presenters)
	Base.init(Presenters)
	Estil.init(Presenters)
  }

  return Presenters
})