define([
  'global',
  'api'
], function (G, Api) {

	var Estil = {}
	
	Estil.init = function (P){ 
	G.on('view:estil:formEmplenat', function (titol,descripcio) {
		Api.createEstil({titol: titol, descripcio: descripcio})
          .then(G.trigger('view:home'))
          .catch(G.trigger.bind(G, 'api:login:error'))
          .done()
	})
	
	G.on('view:estil:mostrarEstils',function(){
		Api.mostrarEstils(function(outputData){
			G.trigger('view:estils:mostrarEstilsResponse',outputData)
		})
			.then()
			.catch(G.trigger.bind(G, 'api:mostrarEstils:error'))
			.done()
	})
		G.on('view:estil:eliminar',function(EstilId){
			Api.eliminarEstil({estilId:EstilId})
				.then(G.trigger('view:home'))
				.catch(G.trigger.bind(G, 'api:login:error'))
				.done()
		})
		G.on('api:mostrarEstils:error', P.Ui.errorAPI);
	
}
 return Estil;
})

