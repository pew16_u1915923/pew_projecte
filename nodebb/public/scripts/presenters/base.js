define([
  'global',
  'api'
], function (G, Api) {

	var Base = {}
	
	Base.init = function (P){ 
	G.on('view:base:formEmplenat', function (titol,descripcio,url) {
		var files = G.localStorage.getItem("base")
		Api.createBase({titol: titol, descripcio: descripcio, url:url})
          .then(function(){G.trigger('view:home');})
          .catch(G.trigger.bind(G, 'api:createBase:error'))
          .done()
	})
	
	G.on('view:base:showBases', function(){
	    Api.showBases(function(outputData) {
			G.trigger('view:showAllBasesResponse',outputData)
		})
		.then()
		.catch(G.trigger.bind(G,'api:login:error'))
		.done()
	})


	G.on('view:base:eliminar',function(baseid,username){
		Api.eliminarBase({baseId: baseid, username:username})
			.then(function(){G.trigger('view:home');})
			.catch(G.trigger.bind(G, 'api:eliminarBase:error'))
			.done()
	})
	
	G.on('view:base:associarEstil',function(baseid,estilNom){
		Api.associarEstil({baseId:baseid, estilNom:estilNom})
			.then(function(){G.trigger('view:home');})
			.catch(G.trigger.bind(G, 'api:associarEstil:error'))
			.done()
	})
	G.on('view:base:mostraBaseForm',function(baseid){

		var attribs = {
			singleBase : true,
			baseid: baseid
		}
		Api.showBases(function(outputData){
			G.trigger('view:showBaseResponse',outputData)
		},attribs)
			.then(function(){G.trigger('view:home');})
			.catch(G.trigger.bind(G, 'api:showBases:error'))
			.done()
	})
	G.on('view:base:desassiciarEstil',function(baseid,estilNomIn){
		Api.desassociarEstil({baseID:baseid,estilNom:estilNomIn})
			.then(function(){G.trigger('view:home');})
			.catch(G.trigger.bind(G, 'api:showBases:error'))
			.done()
	})

	}
	G.on('createBase:uploadFile',function(titol,descripcio,url)
	{
		var data = {titol: titol, descripcio: descripcio, url:url}
		var formData = new FormData();
		formData.append('base',data);
		Api.createBase({titol: titol, descripcio: descripcio, url:url})
		//Api.createBase(data)
			.then(function(){G.trigger('view:home');})
			.catch(G.trigger.bind(G, 'api:createBase:error'))
			.done()
	})

	return Base;
})

