/**
 * Created by ABC on 11/06/2016.
 */
define([
    '../global',
    'jquery',
    'views/base/baseinfo',
    'collections/c_userbases'],
    function (G, $,BaseInfoView,BaseCollection){

        
        var PerfilUi = {};
        var colectionBase;
        var userBasesView;

        PerfilUi.init = function(){

        },
        PerfilUi.switchContent = function (widget) {
            var $listcontent = $('#listcontent')
            var args = Array.prototype.slice.call(arguments)
                switch (widget) {
                    case 'pestanya1' :
                    {
                        $listcontent.html(userBasesView.render.apply(userBasesView, args).el)
                        userBasesView.delegateEvents()
                        break
                    }
                }

            },

            PerfilUi.mostraUserBases = function(userid){
                colectionBase = new BaseCollection(userid);
                userBasesView = new BaseInfoView({collection: colectionBase})
                colectionBase.fetch({
                    success:PerfilUi.switchContent.bind(PerfilUi,'pestanya1'),
                    error:PerfilUi.UserBaseError
                })
                
            },

        PerfilUi.UserBaseError = function()
        {
            alert("error al mostrar les bases del usuari.")
        }


        G.on('view:perfilUi:basesUser',PerfilUi.mostraUserBases)
        return PerfilUi;
    })
