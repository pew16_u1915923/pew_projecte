define([
    '../global',
    'jquery',
    'collections/c_orders',
    'collections/c_estils',
	'collections/c_bases',
    '../collections/c_estilsbase',
    'views/user/login',
    'views/user/perfil',
    'views/user/signup',
    'views/header',
    'views/order/vl_orders',
	'../views/homenotlogged',
	'../views/base/createbase',
	'../views/base/baseinfo',
    '../views/base/associarestil',
    'views/base/dessasociar',
    '../views/base/mostrabase',
	//'views/base/tl_song',
    '../views/base/mostraestilsinfo',
    '../views/base/mostraestils',
	/*'views/user/forgotPass',*/
	'../views/estils/createestil',
    '../views/estils/estilinfo',
    '../views/base/eliminarbase',
    '../views/estils/eliminarestil'],
  function (G, $, CollectionOrder,CollectionEstils,CollectionBases,CollectionsEstilsBase, UserLogin, UserPerfil, UserSignup, HeaderView, OrdersView,/*perfilBase,*/ homeNotLogged,formCreateBase,listaBasesView,associarEstil,desassociarForm,mostraUnaBase,EstilsBase,EstilBaseForm,formCreateEstil,listaEstilsView,eliminarBaseForm,eliminarEstilForm/*, forgotPassMail*/) {

    var Ui = {}

    // Views that can be rendered at #content
    var loginView = new UserLogin()
    var signupView = new UserSignup()

	var homeNotLoggedView = new homeNotLogged()
	var createBaseForm = new formCreateBase()
	var perfilView = new UserPerfil()
	var createEstilForm = new formCreateEstil()
	var orderList = new CollectionOrder()
    var estilsBase;
    var desassocairView = new desassociarForm()
	var basesList = new CollectionBases()
    var estilList = new CollectionEstils()
	var ordersView = new OrdersView({collection: orderList})
	var basesView = new listaBasesView({collection: basesList,G:G})
    var estilsView = new listaEstilsView({collection: estilList})
    var EstilsBaseView;
    var EstilBaseFormView = new EstilBaseForm()
    var eliminarBaseView = new eliminarBaseForm()
    var associarEstilView = new associarEstil()
    var mostraUnabaseView = new  mostraUnaBase()
	//var perfilBaseView = new perfilBase()
    var eliminarEstilView = new eliminarEstilForm()
	//var forgotPassView = new forgotPassMail()
  
    var $content = $('#content')
	G.localStorage.setItem('localWidget','homeNotLogged')
	G.localStorage.setItem('context', 'homeNotLogged')
   var headerView = new HeaderView({el: '#header'})
 
    Ui.switchContent = function(widget) {
        if(!G.localStorage.hasItem('logincheck')
            || !G.localStorage.getItem('logincheck')) {
            G.localStorage.setItem('logincheck', false);
            G.trigger('view:logincheck:request');
        }
        var args = Array.prototype.slice.call(arguments)
	    G.localStorage.setItem('localWidget', widget)
	    G.localStorage.setItem('context', widget)
	    headerView = new HeaderView({el: '#header'})
        headerView.setUserData(G.localStorage.getItem('user'))
        headerView.delegateEvents()
        args.shift()
      switch (widget) {

	    case 'home' :{
		 $content.html(homeNotLoggedView.render.apply(homeNotLoggedView,args).el)
		 homeNotLoggedView.delegateEvents()
            break
		}
	    case 'homeNotLogged' :{
		 $content.html(homeNotLoggedView.render.apply(homeNotLoggedView,args).el)
		 homeNotLoggedView.delegateEvents()
		 break
		 }
        case 'login': {
          $content.html(loginView.render.apply(loginView, args).el)
          loginView.delegateEvents()
          break
        }

        case 'profile':
        {
            $content.html(perfilView.render.apply(perfilView, args).el)
            perfilView.setUserData(G.localStorage.getItem("user"))
            perfilView.delegateEvents()
            break
        }

        case 'signup': {
          $content.html(signupView.render.apply(signupView, args).el)
          signupView.delegateEvents()
          break
        }
		
		/*case 'forgotPass': {
          $content.html(passView.render.apply(passView, args).el)
          passView.delegateEvents()
          break
        }*/
		case 'formCreateBase':
		{
		  $content.html(createBaseForm.render.apply(createBaseForm, args).el)
          createBaseForm.delegateEvents()
          break
		}
		case 'bases': {
		$content.html(basesView.render.apply(basesView, args).el)
          basesView.delegateEvents()
			 break
			
		}
        case 'estils':{
            $content.html(estilsView.render.apply(estilsView, args).el)
            estilsView.delegateEvents()
            break
          }
		case 'formCreateEstil':
		{
			$content.html(createEstilForm.render.apply(createEstilForm, args).el)
			createEstilForm.delegateEvents()
			break
		}
          case 'formEliminarBase':
          {
              $content.html(eliminarBaseView.render.apply(eliminarBaseView, args).el)
              eliminarBaseView.delegateEvents()
              break
          }
		  
		 case 'UserForgotPass':
          {
              $content.html(forgotPassView.render.apply(forgotPassView, args).el)
              forgotPassView.delegateEvents()
              break
          }
          case 'associarEstil':
          {
              $content.html(associarEstilView.render.apply(associarEstilView, args).el)
              associarEstilView.delegateEvents()
              break
          }
          case 'desassociarEstil':
          {
              $content.html(desassocairView.render.apply(desassocairView, args).el)
              desassocairView.delegateEvents()
              break
          }
          case 'mostrarUnaBase':
          {
              $content.html(mostraUnabaseView.render.apply(mostraUnabaseView, args).el)
              mostraUnabaseView.delegateEvents()
              break
          }
		  /*case 'perfilBase':
          {
              $content.html(perfilBaseView.render.apply(perfilBaseView, args).el)
              perfilBaseView.delegateEvents()
              break
          }*/
          case 'mostraEstilsForm':
          {
              $content.html(EstilBaseFormView.render.apply(EstilBaseFormView, args).el)
              EstilBaseFormView.delegateEvents()
              break
          }
          case 'estilsbase':
          {
              $content.html(EstilsBaseView.render.apply(EstilsBaseView, args).el)
              EstilsBaseView.delegateEvents()
              break
          }
          case 'formEliminarEstil':
          {
              $content.html(eliminarEstilView.render.apply(eliminarEstilView, args).el)
              eliminarEstilView.delegateEvents()
              break
          }
        case 'orders': {
          orderList.fetch({
		     data: {id:1}, 
            success: function() {
              $content.html(ordersView.render.apply(ordersView, args).el)
                ordersView.delegateEvents()
            },
            error: Ui.error
          });
          break
        }
      }
    }

    Ui.init = function () {
        G.localStorage.setItem('logincheck', false);
        G.trigger('view:logincheck:request');
        var user = G.localStorage.getItem('user')
        if(user) {
            headerView.setUserData(user);
        } else {

        }
        Ui.showHome();
    }

    Ui.showHome = function () {
        Ui.switchContent('homeNotLogged')
    }
	Ui.showLogin = function(){
		Ui.switchContent('login')
	}
	
	/*Ui.forgotPass = function(){
		Ui.switchContent('forgotPass')
	}*/
	
    Ui.showSignup = function () {
      Ui.switchContent('signup')
    }

    Ui.showUserPerfil = function() {
        Ui.switchContent('profile')
    }

	Ui.showFormCreateBase= function(){
	  Ui.switchContent('formCreateBase')
	}
	Ui.showFormCreateEstil = function(){
	 Ui.switchContent('formCreateEstil')
	}
	Ui.showAllBasesResponse = function(data){
		basesList.fetch({
			success: Ui.switchContent.bind(Ui,'bases'),
			error: Ui.error,
			//data: {id:1}
		});
	}
      Ui.mostrarEstilsResponse = function(data){
         estilList.fetch({
             success:Ui.switchContent.bind(Ui,'estils'),
             error:Ui.error,
         });
      }
    Ui.showOrders = function () {
        orderList.fetch({
            success: Ui.switchContent.bind(Ui, 'orders'),
            error: Ui.errorAPI
        });
    }
      Ui.mostraEstils = function(baseid){
          estilsBase = new CollectionsEstilsBase(baseid);
          EstilsBaseView = new EstilsBase({collection: estilsBase})
          estilsBase.fetch({
              success:Ui.switchContent.bind(Ui,'estilsbase'),
              error:Ui.baseError
          })
          Ui.switchContent('')
      }
    Ui.showEliminarForm = function() {
        Ui.switchContent('formEliminarBase');
    }
      Ui.showAssociarForm =function(){
          Ui.switchContent('associarEstil');
      }
      Ui.showDesassociarForm = function(){
          Ui.switchContent('desassociarEstil');
      }
      Ui.showUnaBase = function(){
          Ui.switchContent('mostrarUnaBase');
      }
      Ui.showUnaBaseResponse = function(base)
      {
		  //var b = JSON.parse(JSON.stringify(base));
		  var array = $.map(base, function(el) {return el});
          alert(JSON.stringify(base));
      }
      Ui.eliminarEstilForm =function(){
          Ui.switchContent('formEliminarEstil')
      }
      Ui.mostraEstilsForm =function(){
          Ui.switchContent('mostraEstilsForm')
      }
    Ui.errorBackbone  = function (data, res) {
        alert("Error: " + res.responseJSON.error.message)
    }

    // This always receive a JSON object with a standard API error
    Ui.error = function (err) {
        alert("Error: " + err.message)
    }
    Ui.baseError = function(){
        alert("Error: Aquesta base no existeix o te algun problema");
    }
    // This always receive a jQuery error object from an API call
    Ui.errorAPI = function (res) {
        if(res.responseJSON == undefined)
             alert("APIError: " + res.responseText)
        else
            alert("APIError: " + res.responseJSON.error.message)
    }

    G.on('presenter:switch-ui:home', Ui.showHome)
	G.on('view:login', Ui.showLogin)
	//G.on('view:forgotPass', Ui.UserForgotPass)
	G.on('view:home',Ui.showHome)
	G.on('view:createBase',Ui.showFormCreateBase)
    G.on('view:perfil',Ui.showUserPerfil)
	G.on('view:createEstil',Ui.showFormCreateEstil)
	G.on('view:showAllBasesResponse',Ui.showAllBasesResponse)
    G.on('view:eliminarForm',Ui.showEliminarForm)
    G.on('view:estils:mostrarEstilsResponse',Ui.mostrarEstilsResponse)
    G.on('view:associarForm',Ui.showAssociarForm)
    G.on('view:base:mostraBase',Ui.showUnaBase)
    G.on('view:showBaseResponse',Ui.showUnaBaseResponse)
    G.on('view:desassociarEstilForm',Ui.showDesassociarForm)
    G.on('view:eliminarEstilForm',Ui.eliminarEstilForm)
    G.on('view:base:mostraEstilsBase',Ui.mostraEstils)
    G.on('view:EstilsBasesForm', Ui.mostraEstilsForm)
    G.on('api:eliminarBase:error',Ui.errorAPI)
    return Ui
  })
