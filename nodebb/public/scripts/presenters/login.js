define([
  'global',
  'api'
], function (G, Api) {

  var Login = {}
  var timeout = 0;

  Login.init =  function (P) {
      
      //CHECK LOGGED

      G.on('view:logincheck:request', function () {
          G.localStorage.setItem('logincheck',true);
          Api.checkLogged()
              .then(G.trigger.bind(G, 'api:logincheck:successful'))
              .catch(G.trigger.bind(G, 'api:logincheck:error'))
              .done()
      })

      G.on('api:logincheck:successful', function(data){
          if(data.msg){
              G.trigger('api:logincheck:error');
          } else {
              G.localStorage.setItem('user', data);
              G.trigger('presenter:switch-ui:home');
          }
      })

      G.on('api:logincheck:error', function(){
         if(G.localStorage.hasItem('user')) {
              G.localStorage.removeItem('user');
          }
      })
      
      //LOGIN

      G.on('view:login:request', function (username, password) {
        Api.login({username: username, password: password})
          .then(G.trigger.bind(G, 'api:login:successful'))
          .catch(G.trigger.bind(G, 'api:login:error'))
          .done()
      })

      G.on('api:login:successful', function (user) {
        G.localStorage.setItem('user', user);
        G.trigger('presenter:switch-ui:home')
      })

      G.on('api:login:error', P.Ui.errorAPI)

      //LOGOUT

      G.on('view:logout:request', function () {
          Api.logout()
              .then(G.trigger.bind(G, 'api:logout:successful'))
              .catch(G.trigger.bind(G, 'api:logout:error'))
              .done()
      })

      G.on('api:logout:successful', function(data){
          console.log("Logout successful: ",data.msg);
          timeout = 0;
          G.localStorage.removeItem('user');
          G.trigger('presenter:switch-ui:home');
      })

      G.on('api:logout:error', function(data){
          console.log("Error logging out, retrying... ",timeout,"/From server: ",data.msg);
          timeout++;
          if(G.localStorage.hasItem('user')) {
              G.localStorage.removeItem('user');
          }
          if(timeout <= 2) {
              G.trigger('view:logout:request');
          } else {
              console.log("Could not log out from the server, maybe it's offline.")
              timeout = 0;
          }
          G.trigger('presenter:switch-ui:home');
      })

      //NEW USER
      
      G.on('view:signup:request', function (data) {
        Api.signup(data)
          .then(G.trigger.bind(G, 'api:signup:successful'))
          .catch(G.trigger.bind(G, 'api:signup:error'))
          .done()
      })

      G.on('api:signup:successful', function (user) {
        G.trigger('presenter:switch-ui:home')
      })

      G.on('api:signup:error',  P.Ui.errorAPI)

      //PROFILE
      
      G.on('view:profile:request', function (username) {
          Api.profile({username:username})
              .then(G.trigger.bind(G,'api:profile:success'))
              .catch(G.trigger.bind(G, 'api:profile:error'))
      })

      G.on('api:profile:success',function(user){
          G.localStorage.setItem('profile',user)
          G.trigger('localstorage:set:profile')
      })
      G.on('api:profile:error', P.Ui.errorAPI)
    }

  return Login

})