define(['jquery', 'promises','global'], function ($, P,G) {
  var Api = {};

    // Users
    
    //Login
  Api.login = function (data) {
    return new P(function(resolve, reject) {
      $.ajax({
        url: '/api/users/login',
        dataType: 'json',
        type: 'post',
        contentType: 'application/json',
        data: JSON.stringify(data),
        processData: false,
        success: resolve,
        error: reject
      });
    })
  };

    //New User
  Api.signup = function (data) {
    return new P(function (resolve, reject) {
      $.ajax({
        url: '/api/users',
        dataType: 'json',
        type: 'post',
        contentType: 'application/json',
        data: JSON.stringify(data),
        processData: false,
        success: resolve,
        error: reject
      });
    });
  };
    
    //Get Perfil

    Api.profile = function (data) {
        return new P(function (resolve, reject) {
            var user = G.localStorage.getItem('user');
            if (!user) {
                return reject(new Error("API call needs user authenticated"))
            }
            var token = user.jwt;
            if(!data.hasOwnProperty("username")){
                return reject(new Error("API call needs the username"))
            }
            $.ajax({
                url: '/api/users/profile',
                dataType: 'json',
                type: 'get',
                contentType: 'application/json',
                data: JSON.stringify(data),
                processData: false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + token);
                },
                success: resolve,
                error: reject
            });
        });
    };
    
    //Check logged in
  Api.checkLogged = function () {
      return new P(function (resolve, reject) {
          $.ajax({
              url: '/api/users/check',
              dataType: 'json',
              type: 'get',
              contentType: 'application/json',
              processData: false,
              success: resolve,
              error: reject
          });
      });      
  };
    
    //Logout
  Api.logout = function () {
      var token = G.localStorage.getItem('user').jwt;
      return new P(function (resolve, reject) {
          $.ajax({
              url: '/api/users/logout',
              dataType: 'json',
              type: 'get',
              contentType: 'application/json',
              processData: false,
              beforeSend: function (xhr) {
                  xhr.setRequestHeader("Authorization", "Bearer " + token);
              },
              success: resolve,
              error: reject
          });
      });
  };

  Api.createBase = function(data) {
		return new P(function (resolve,reject) {
            var user = G.localStorage.getItem('user');
            
            if (!user) {
                return reject(new Error("API call needs user authenticated"))
            }
            var token = user.jwt;
			$.ajax({
				url:'/api/base/create',
				type:'post',
				contentType: 'application/json',
				data:  JSON.stringify(data),
				procecssData:false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + token);
                },
				success:resolve,
				error:reject
			});
		});
  };

    //CREATE AMB FORM DATA
    /*Api.createBase = function(data) {
        return new P(function (resolve, reject) {
            var user = G.localStorage.getItem('user');

            if (!user) {
                return reject(new Error("API call needs user authenticated"))
            }
            var token = user.jwt;
            $.ajax({
                url: '/api/base/create',
                type: 'post',
                contentType: false,
                data: data,
                procecssData: false,
                success: resolve,
                error: reject
            });
        });
    };*/
  Api.showBases = function(handleData, dataIn){
  
	return new P(function (resolve,reject) {
			$.ajax({
				url:'api/base/showAllBases',
				dataType:'json',
				type:'post',
				contentType: 'application/json',
                data: JSON.stringify(dataIn),
				processData: false,
				success:function(data) {handleData(data);},
				error:reject
			});
		});
  };

    Api.mostrarEstils = function(handleData){

        return new P(function (resolve,reject) {
            $.ajax({
                url:'/api/estil/mostraEstils',
                dataType:'json',
                type:'get',
                contentType: 'application/json',
                processData: false,
                success:function(data) {handleData(data);},
                error:reject
            });
        });
    };
  
  Api.createEstil = function(data){
  return new P(function (resolve,reject) {
      var user = G.localStorage.getItem('user');

      if (!user) {
          return reject(new Error("API call needs user authenticated"))
      }
      var token = user.jwt;
			$.ajax({
				url:'api/estil/create',
				dataType: 'json',
				type:'post',
				contentType: 'application/json',
				data: JSON.stringify(data),
                procecssData:false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + token);
                },
                success:resolve,
				error:reject
			});
		});
	};
    
    Api.eliminarBase = function(data){
        return new P(function (resolve,reject) {
            var user = G.localStorage.getItem('user');

            if (!user) {
                return reject(new Error("API call needs user authenticated"))
            }
            var token = user.jwt;
            $.ajax({
                url:'/api/base/delete',
                dataType: 'json',
                type:'delete',
                contentType: 'application/json',
                data: JSON.stringify(data),
                procecssData:false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + token);
                },
                success:resolve,
                error:reject
            });
        });
    };

    Api.eliminarEstil = function(data){
        return new P(function (resolve,reject) {
            var user = G.localStorage.getItem('user');

            if (!user) {
                return reject(new Error("API call needs user authenticated"))
            }
            var token = user.jwt;
            $.ajax({
                url:'/api/estil/delete',
                dataType: 'json',
                type:'delete',
                contentType: 'application/json',
                data: JSON.stringify(data),
                procecssData:false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + token);
                },
                success:resolve,
                error:reject
            });
        });
    };


    Api.associarEstil =function(data){
        return new P(function (resolve,reject) {
            var user = G.localStorage.getItem('user');

            if (!user) {
                return reject(new Error("API call needs user authenticated"))
            }
            var token = user.jwt;
            $.ajax({
                url:'/api/base/associarEstil',
                dataType: 'json',
                type:'post',
                contentType: 'application/json',
                data: JSON.stringify(data),
                procecssData:false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + token);
                },
                success:resolve,
                error:reject
            });
        });
    };

    Api.desassociarEstil =function(data){
        return new P(function (resolve,reject) {
            var user = G.localStorage.getItem('user');

            if (!user) {
                return reject(new Error("API call needs user authenticated"))
            }
            var token = user.jwt;
            $.ajax({
                url:'/api/base/desassociarEstil',
                dataType: 'json',
                type:'delete',
                contentType: 'application/json',
                data: JSON.stringify(data),
                procecssData:false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + token);
                },
                success:resolve,
                error:reject
            });
        });
    };
    
  Api.getOrders = function () {
    return new P(function (resolve, reject) {
      var user = Backbone.localStorage.getItem('user');
      if (!user) {
        return reject(new Error("API call needs user authenticated"))
      }
      var token = user.jwt;
      $.ajax({
        url: '/api/users/self/orders',
        dataType: 'json',
        type: 'get',
        contentType: 'application/json',
        processData: false,
        beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        success: resolve,
        error: reject
      });
    });
  };

  return Api;
});
