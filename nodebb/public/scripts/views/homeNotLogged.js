define([
  'global',
  'jquery',
  // Using the Require.js text! plugin, we load raw text
  // which will be used as our views primary template
  'text!/templates/home.html'
], function(G, $, t_home) {

	var homeNotLogged = G.Backbone.View.extend({
		className: 'container',
	 initialize: function () {
      this.template = G._.template(t_home)
	},
	
	render: function() {
	this.$el.html(this.template({bases: this.collection}))
      return this
	},
	
	  events: {
    'click #crearBase': 'crearBase',
	'click #crearEstil': 'crearEstil',
	'click #MostraBases': 'mostrarBases',
	'click #eliminarBase':'eliminarBase',	  
	'click #mostrarEstils':'mostrarEstils',
	'click #associarClaseBase':'mostraFormAssociar',
	'click #mostraBase':'mostraUnaBase',
	'click #Dessasociar':'desassociarEstil',
	'click #eliminarEstil':'eliminarEstil',
    'click #estilsBase':'estilsBase',
	},
	estilsBase:function(){
		G.trigger('view:EstilsBasesForm');
	},
	desassociarEstil:function(){
		G.trigger('view:desassociarEstilForm');
	},
	crearBase:function(){
	 G.trigger('view:createBase');
	},
	crearEstil:function(){
	 G.trigger('view:createEstil');
	},
	mostrarBases:function(){
	  G.trigger('view:base:showBases');
	}, 
	mostraUnaBase:function(){
		G.trigger('view:base:mostraBase');
	},

	eliminarBase:function(){
		G.trigger('view:eliminarForm');
	}, 
	mostrarEstils:function(){
		G.trigger('view:estil:mostrarEstils');		
	},
	mostraFormAssociar:function(){
		G.trigger('view:associarForm');
	},
	eliminarEstil:function(){
		G.trigger('view:eliminarEstilForm');
	}
})
return homeNotLogged;
})