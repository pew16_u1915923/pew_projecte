define([
  'global',
  // Using the Require.js text! plugin, we load raw text
  // which will be used as our views primary template
  'text!/templates/header.html'
], function(G, t_header) {

  var userData = {}
  var contexto = {};

  var Header = G.Backbone.View.extend({

    initialize: function () {
      this.template = G._.template(t_header)
      G.on('localstorage:set:user', this.setUserData.bind(this))
	  G.on('localstorage:set:context',this.setContext.bind(this))
	  G.on('localstorage:remove:user', this.removeUserData())
    },

    render: function() {
      this.$el.html(this.template({user: userData, context: contexto}))
      return this
    },
	
	setContext: function(cont)
	{	
		contexto = cont;
		this.render()
	},
    setUserData: function(user) {
		if(user) {
			userData = user
			this.render()
		}
    },
	  
	removeUserData : function(){
		this.userData = {};
	},  
	  
	  events: {
		  
	'click .aSignIn': 'expandSignIn',
	'click .aCreateAccount': 'expandSignUp',
	'click .aMyProfile': 'expandProfile',
	'click .aLogOut': 'throwLogout',
	'click .home': 'goHome'
	},
	  
	expandSignIn:function(){
	 G.trigger('view:login');
	},
	  
	expandSignUp:function(){
	 G.trigger('view:signup');
	},
	  
	expandProfile:function(){
		G.trigger('view:perfil');
	},
	  
	throwLogout:function(){
		G.trigger('view:logout:request');
	},

	goHome:function(){
	 G.trigger('view:home');
	}
	
  })
  // Our module now returns our view
  return Header;
})