define([
  'global',
  // Using the Require.js text! plugin, we are loaded raw text
  // which will be used as our views primary template
  'text!/templates/user/signup.html'
], function(G, t_signup) {

  var UserSignup = G.Backbone.View.extend({

    className: 'container',

    template: G._.template(t_signup),

    init: function () {
    },

    events: {
      'click #btn-signup': 'submit',
      'click #btn-fbsignup': 'fblink',
	  'click .aSignIn': 'expandSignIn'
    },

    submit: function() {

      var data = {
        username: this.$('[name=username]').val(),
        truename: this.$('[name=truename]').val(),
        email: this.$('[name=email]').val(),
        password: this.$('[name=passwd]').val(),
		password2: this.$('[name=reppasswd]').val()
      }
	  if(data.password == data.password2)
			G.trigger('view:signup:request', data)
	  else
			alert("Error");
    },

    fblink: function () {
      alert("Encara no esta implementat...")
    },
	
	expandSignIn:function(){
	 G.trigger('view:login');
	},

    render: function() {
      this.$el.html(this.template())
      return this
    }
  })

  return UserSignup
})