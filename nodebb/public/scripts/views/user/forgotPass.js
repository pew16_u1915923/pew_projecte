define([
  'global',
  // Using the Require.js text! plugin, we are loaded raw text
  // which will be used as our views primary template
  'text!/templates/user/forgotpass.html'
], function(G, t_forgotPass) {

  var forgotPass = G.Backbone.View.extend({

    className: 'container',

    template: G._.template(t_forgotPass),

    init: function () {
    },

    events: {
      'click #btn-send': 'send'
    },

    submit: function() {
		alert("hola");
		/*var link = "mailto:pau_m_p@hotmail.com"
				+ "?cc=myCCaddress@example.com"
				+ "&subject=" + escape("prova")
				+ "&body=" + escape(document.getElementById('correcte').value)
		;

		window.location.href = link;*/
    },

    render: function() {
      this.$el.html(this.template())
      return this
    }
  })

  return forgotPass
})