/**
 * Created by Ytturi on 18/04/2016.
 */
define([
    'global',
    // Using the Require.js text! plugin, we load raw text
    // which will be used as our views primary template
    'text!/templates/user/tl_perfil.html'
], function(G, t_perfil) {

    var userData = {};
    
    var perfilTemplate = G.Backbone.View.extend({
        className: 'container',
        template: G._.template(t_perfil),


        initialize: function () {
            G.on('localstorage:set:profile', this.bindUserData.bind(this));
        },

        events: {
            'click #p1': 'showPest1',
            'click #p2': 'showPest2',
            'click #p3': 'showPest3',
            'click #img_change': 'changeImg'
        },

        render: function() {
            this.$el.html(this.template({user: userData}));
            return this
        },

        setUserData: function(user) {
            userData.username = user.username;
            G.trigger('view:profile:request', user.username);
        },

        bindUserData: function(user){
            if(user) {
                userData = user;
                this.render()
            }
        },

        showPest1: function(){
            G.trigger('view:perfilUi:basesUser',userData.username)
        },

        showPest2: function(){
            G.trigger('view:perfilUi:basesUser',userData.username)
        },

        showPest3: function(){
            G.trigger('view:perfilUi:basesUser',userData.username)
        },

        changeImg : function(){
            alert("Not implemented yet!");
        }

    });
    return perfilTemplate;
});
