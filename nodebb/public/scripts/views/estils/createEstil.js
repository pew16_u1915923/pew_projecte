define([
  'global',
  // Using the Require.js text! plugin, we load raw text
  // which will be used as our views primary template
  'text!/templates/estil/tl_createestil.html'
], function(G, t_createEstil) {

	var createBase = G.Backbone.View.extend({
	 initialize: function () {
      this.template = G._.template(t_createEstil)
	},
	
	render: function() {
	this.$el.html(this.template())
      return this
	},
	
	events: {
    'click .enviaEstil' :'enviarEstil',
	},
	
	enviarEstil:function(){
      G.trigger('view:estil:formEmplenat', this.$('#titolform').val(), this.$('#descripcioform').val())
	},
})
return createBase;
})