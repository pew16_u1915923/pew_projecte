define([
    'global',
    'jquery',
    // Using the Require.js text! plugin, we load raw text
    // which will be used as our views primary template
    'text!/templates/estil/tl_eliminarestil.html'
], function(G,$,t_EliminarEstil) {

    var baseTemplate = G.Backbone.View.extend({
        className: 'container',
        initialize: function () {
            this.template = G._.template(t_EliminarEstil)
        },

        render: function() {
            this.$el.html(this.template());
            return this
        },

        events: {
            'click  .eliminarEstil':'eliminarEstil',
        },

        eliminarEstil: function(){
            var usuari = G.localStorage.getItem("user");
            if(usuari)
                G.trigger('view:estil:eliminar',this.$('#estilid').val(),usuari.username)
            else {
                alert("no estas conectat");
            }
        }

    })
    return baseTemplate;
})/**
 * Created by ABC on 08/06/2016.
 */
