define([
  'global',
  'jquery',
  // Using the Require.js text! plugin, we load raw text
  // which will be used as our views primary template
  'text!/templates/base/tl_base.html'
], function(G,$,t_base) {

	var baseTemplate = G.Backbone.View.extend({
	className: 'container',
	 initialize: function () {
     	this.template = G._.template(t_base)
	 },

	render: function() {
	this.$el.html(this.template({bases: this.collection}))
    	return this
	},


	events: {
			'click .aReproduir': 'reproduir',
			'click .aEliminar': 'eliminar',
		    'click .trBase':'infobaseid',
	},

	eliminar: function() {
		//aqet tros de codi no agafa la id que ha d'agafar
		var base = G.localStorage.getItem("baseid");
		var usuari = G.localStorage.getItem("user");
		if(usuari)
		{
			G.trigger('view:base:eliminar',base,usuari.username)
		}
		else {
			alert("no tens permís per eliminar aquesta base");	
		}
    },
		infobaseid:function(e){
			var idaux = e.currentTarget.cells[0].innerHTML;
			G.localStorage.setItem("baseid",idaux);

		},
	reproduir: function() {
	 //solucio per defecte
     //document.getElementById('audio_id').play();
	 //la intencio es que despres de premer reproduir anem a la pagina de la canço i es reprodueixi alla
	 G.trigger('view:base:song');
    },

	desplegarMenu:function()
	{
		var Cells = this.$('.userid');
		var id  = Cells[0].innerHTML;
		var usuari = G.localStorage.getItem("user");
		if(usuari)
		{
			//aqui aniria el codi per agafar el usuari, si el usuari es el mateix que el de la canso
			//sortiran unes opcions, en canvi si no es el usuari propietari nomes podra visualitzar la informació o escoltarla.
		}
		else {
			alert("no hi ha usuari");
			
		}
	}

})
return baseTemplate;
})