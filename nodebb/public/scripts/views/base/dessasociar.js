/**
 * Created by ABC on 05/06/2016.
 */
define([
    'global',
    'jquery',
    // Using the Require.js text! plugin, we load raw text
    // which will be used as our views primary template
    'text!/templates/base/tl_dessasociar.html'
], function(G,$,t_desassociar) {

    var baseTemplate = G.Backbone.View.extend({
        className: 'container',
        initialize: function () {
            this.template = G._.template(t_desassociar)
        },

        render: function() {
            this.$el.html(this.template());
            return this
        },

        events: {
            'click  .enviaDesassociar':'enviaDesassociar',
        },
        enviaDesassociar:function(){
            G.trigger('view:base:desassiciarEstil', this.$('#baseid').val(), this.$('#estilId').val())
        },


    })
    return baseTemplate;
})