/**
 * Created by ABC on 09/06/2016.
 */
define([
    'global',
    'jquery',
    // Using the Require.js text! plugin, we load raw text
    // which will be used as our views primary template
    'text!/templates/base/tl_mostraestils.html'
], function(G,$,tl_mostraEstils) {

    var baseEstilsTemplate = G.Backbone.View.extend({
        className: 'container',
        initialize: function () {
            this.template = G._.template(tl_mostraEstils)
        },

        render: function() {
            this.$el.html(this.template());
            return this
        },

        events: {
            'click  .enviaBaseEstils':'enviaBaseEstils',
        },
        enviaBaseEstils: function(){
            G.trigger('view:base:mostraEstilsBase',this.$('#baseid').val())
        }

    })
    return baseEstilsTemplate;
})