/**
 * Created by ABC on 06/06/2016.
 */
/**
 * Created by ABC on 05/06/2016.
 */
define([
    'global',
    'jquery',
    // Using the Require.js text! plugin, we load raw text
    // which will be used as our views primary template
    'text!/templates/base/tl_mostrabase.html'
], function(G,$,t_associarEstil) {

    var baseTemplate = G.Backbone.View.extend({
        className: 'container',
        initialize: function () {
            this.template = G._.template(t_associarEstil)
        },

        render: function() {
            this.$el.html(this.template());
            return this
        },

        events: {
            'click  .enviaBaseMostra':'enviaBaseMostra',
        },
           enviaBaseMostra: function(){
               G.trigger('view:base:mostraBaseForm',this.$('#baseid').val())
           }     

    })
    return baseTemplate;
})