define([
  'global',
  // Using the Require.js text! plugin, we load raw text
  // which will be used as our views primary template
  'text!/templates/base/tl_createbase.html'
], function(G, t_createBase) {

	var fileColection = [];
	var createBase = G.Backbone.View.extend({

	 initialize: function () {
      this.template = G._.template(t_createBase)
	},
	
	render: function() {
	this.$el.html(this.template())
      return this
	},
	
	events: {
    'click .enviaCanso' :'enviaCanso',
	'change #urlform': 'uploadbase',
	},
	uploadbase:function(e) {
		var files = e.target.files;
		var file = null;
		if (files[0] != null)
			file = files[0]
		fileColection = [];
		fileColection.push(file);
	},
		enviaCanso:function(){
			var file = fileColection[0];
			//G.trigger('createBase:uploadFile', this.$('#titolform').val(), this.$('#descripcioform').val(),file)
		G.trigger('createBase:uploadFile', this.$('#titolform').val(), this.$('#descripcioform').val(), this.$('#urlform').val())
	},
})
return createBase;
})