define([
    'global',
    // Using the Require.js text! plugin, we are loaded raw text
    // which will be used as our views primary template
    'text!/templates/estil/tl_estil.html'
], function(G, tl_estils) {

    var EstilsListView = G.Backbone.View.extend({

        className: 'container',

        initialize: function() {
            this.template = G._.template(tl_estils)
        },

        render: function() {
            this.$el.html(this.template({estils: this.collection}))
            return this
        }

    });

    return EstilsListView
});
