/**
 * Created by Ytturi on 18/04/2016.
 */
define([
    'global',
    // Using the Require.js text! plugin, we load raw text
    // which will be used as our views primary template
    'text!/templates/base/tl_song.html'
], function(G, tl_song) {

    var userData = {}
    
    var perfilTemplate = G.Backbone.View.extend({
        className: 'container',
        initialize: function () {
            this.template = G._.template(tl_song)
            G.on('localstorage:set:user', this.setUserData.bind(this))
        },

        render: function() {
            this.$el.html(this.template())
            return this
        },

        setUserData: function(user) {
            userData = user
            this.render()
        }
    })
    return perfilTemplate;
})
