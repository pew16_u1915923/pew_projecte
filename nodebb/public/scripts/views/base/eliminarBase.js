define([
    'global',
    'jquery',
    // Using the Require.js text! plugin, we load raw text
    // which will be used as our views primary template
    'text!/templates/base/tl_eliminarbase.html'
], function(G,$,t_EliminarBase) {

    var baseTemplate = G.Backbone.View.extend({
        className: 'container',
        initialize: function () {
            this.template = G._.template(t_EliminarBase)
        },

        render: function() {
            this.$el.html(this.template());
            return this
        },
        
        events: {
            'click  .eliminarBase':'eliminarBase',
        },
        
        eliminarBase: function(){
            var usuari = G.localStorage.getItem("user");
            if(usuari)
            G.trigger('view:base:eliminar',this.$('#baseid').val(),usuari.username)
            else {
                alert("no estas conectat");
            }
        }

    })
    return baseTemplate;
})