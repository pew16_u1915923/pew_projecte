define(['backbone'],
  function(Backbone){
    var BaseModel = Backbone.Model.extend({
        urlRoot: "/api/bases",
    
	defaults:
	{
		name: 'defaultName',
		url: '/audio/',
		
	},
    // Return the model for the module
	});
    return BaseModel;
});