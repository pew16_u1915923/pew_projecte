define(['backbone'],
  function(Backbone){
    var EstilModel = Backbone.Model.extend({
        urlRoot: "/api/estils",

	defaults:
	{
		name: 'defaultName',
		description: 'defaultDescription'
	},
    });
    // Return the model for the module
    return EstilModel;
});