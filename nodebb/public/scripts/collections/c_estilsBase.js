define([
    'backbone',
    'models/m_estil'
], function(Backbone, EstilModel){
    var EstilsBaseCollection = Backbone.Collection.extend({
        initialize:function(baseid){
            this.id = baseid;
        },
        model: EstilModel,
        url: function(){
            return '/api/base/estils/'+ this.id
        }
        
    });

    return EstilsBaseCollection;
});