/**
 * Created by ABC on 11/06/2016.
 */
define([
    'backbone',
    'models/m_base'
], function(Backbone, BaseModel){
    var UserBaseCollection = Backbone.Collection.extend({
        initialize:function(userid){
            this.id = userid;
        },
        model: BaseModel,
        url: function(){
            return '/api/user/bases/'+ this.id
        }

    });

    return UserBaseCollection;
});