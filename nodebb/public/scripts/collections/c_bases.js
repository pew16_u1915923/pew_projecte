define([
    'backbone',
    'models/m_base'
], function(Backbone, BaseModel){
    var BaseCollection = Backbone.Collection.extend({
        model: BaseModel,
        url: '/api/base/showAllBases'
    });

    return BaseCollection;
});