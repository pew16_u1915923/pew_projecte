/**
 * Created by ABC on 03/06/2016.
 */
define([
    'backbone',
    'models/m_estil'
], function(Backbone, EstilModel){
    var EstilCollection = Backbone.Collection.extend({
        model: EstilModel,
        url: '/api/estil/mostraEstils'
    });

    return EstilCollection;
});