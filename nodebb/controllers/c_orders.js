module.exports = function (app) {

  var P = app.Promise;
  var db = app.db;

  var util = require('../util');
  var dao = require('../dao')(app);

  var Suser;

  return {
    create: function (req, res) {
      
      Suser = req.session.user;
      util.checkParams(req.body, ['description', 'date']);

      db.sequelize.transaction(function (t) {
        if(!Suser.username){
          util.sendError(500, util.Error.ERR_ENTITY_NOT_FOUND, "Authentication is required");
        }
        
        return dao.User.getByUsername(Suser.username, t)
          .then(function (user) {
            if (!user) util.sendError(500, util.Error.ERR_ENTITY_NOT_FOUND, "User from token does not exist. Authentication is requirec!");
            else return dao.Order.create(req.body, user, t);
          })
      })
        .then(util.jsonResponse.bind(util, res))
        .catch(util.resendError.bind(util, res))
        .done();
    },

    getById: function (req, res) {
      if (!req.params.id) util.stdErr500(res, "Missing parameter 'id'");
      else
        db.Order.find({where: {id: req.params.id, include: [Client, Shop]}})
          .success(util.stdSeqSuccess.bindLeft(res), util.stdSeqError.bindLeft(res))
          .done();
    },

    getOrders: function (req, res) {

      Suser = req.session.user;
      
      if(!Suser.username){
        util.sendError(500, util.Error.ERR_ENTITY_NOT_FOUND, "Authentication is required");
      }
      else {
        dao.Order.getUserOrders(Suser.username, {})
            .then(util.jsonResponse.bind(util, res))
            .catch(util.resendError.bind(util, res))
            .done();
      }
    }
  }
}
