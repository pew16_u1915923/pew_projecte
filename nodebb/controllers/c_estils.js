module.exports = function( app ){

var P = app.Promise;
var db = app.db;
var util = require('../util');
var dao = require('../dao')(app);

 return {
 
 create: function (req, res) {
      util.checkParams(req.body, ['titol','descripcio']);
		 var attribs = {
        titol: req.body.titol,
        description: req.body.descripcio
      }
      db.sequelize.transaction(function (t) {
          return dao.Estil.getByName(attribs.titol, t)
              .then(function (data) {
                  if (data) return util.sendError(500, util.Error.ERR_STYLE_EXISTS, "Aquest estil ja existeix");
                  else return dao.Estil.create(attribs, t);
              })
      })
           .then(util.jsonResponse.bind(util, res))
          .catch(util.resendError.bind(util, res))
          .done();
    },
     mostraEstils: function(req,res){
       return  dao.Estil.mostraEstils({})
             .then(util.jsonResponse.bind(util, res))
             .catch(util.resendError.bind(util, res))
             .done();
     },
     delete:function(req,res){
         util.checkParams(req.body,['estilId']);
         db.sequelize.transaction(function(t){
             //faltaria la logica de poder eliminar estilos que usuarios pueden i cuales no.
             return dao.Estil.delete(req.body.estilId,t)
         }).then(util.jsonResponse.bind(util, res))
             .catch(util.resendError.bind(util, res))
             .done();
     }
}}