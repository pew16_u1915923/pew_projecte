module.exports = function( app ) {

    var P = app.Promise;
    var db = app.db;
    var util = require('../util');
    var dao = require('../dao')(app);
    var fs = app.fs;
    var Suser;

    return {

        setUser: function (user) {

        },
        create: function (req, res) {

            Suser = req.session.user;
          util.checkParams(req.body, ['url', 'titol', 'descripcio']);
            var attribs = {
                url: req.body.url,
                titol: req.body.titol,
                description: req.body.descripcio,
                //file: req.file,
            }
            db.sequelize.transaction(function (t) {
                console.warn("entradintre del sequalize");
                return dao.Base.getBaseByName(attribs.titol, t)
                    .then(function (data) {
                        console.warn("troba la base");
                        if (!data) {
                            if(!Suser.username){
                                util.sendError(500, util.Error.ERR_ENTITY_NOT_FOUND, "Authentication is required");
                            }
                            
                            return dao.User.getByUsername(Suser.username, t)
                                .then(function (user) {
                                    if (!user) return util.sendError(500, util.Error.ERR_ENTITY_NOT_FOUND, "User from token does not exist");
                                    else{
                                       /* var guid = Math.floor((1 + Math.random()) * 0x10000);
                                        console.warn(guid);
                                        var url =  "./audio/" + guid+ ".txt"
                                        console.warn(" lenght del fitxer: "+ attribs.url.length);
                                        fs.writeFile(url,attribs.file,function(err) {
                                        if(err) {
                                            return console.log(err);
                                        }}
                                            )*/
                                       // attribs.url = url;
                                        return dao.Base.create(attribs, user, t);}
                                })
                        }
                        else {
                            return util.throwError(400, util.Error.ERR_BASECREATEERROR, "No s'ha pogut introduir la canso")
                        }
                    })
            })
                .then(util.jsonResponse.bind(util, res))
                .catch(util.resendError.bind(util, res))
                .done();
        },

        getBases: function (req, res) {
            var attribs = {
                singleBase: req.body.singleBase,
                baseid: req.body.baseid,
            }
            db.sequelize.transaction(function (t) {
                return dao.Base.getAllBases(attribs, t)
            })
                .then(util.jsonResponse.bind(util, res))
                .catch(util.resendError.bind(util, res))
                .done();
        },

        delete: function (req, res) {
            Suser = req.session.user;
            util.checkParams(req.body, ['baseId', 'username']);

            
            var attribs = {
                username: req.body.username,
                baseid: req.body.baseId,
            }
            db.sequelize.transaction(function (t) {
                return dao.User.getByUsername(attribs.username, t)
                    .then(function (user) {
                        if (user && user.dataValues.id ==Suser.id) {
                            return dao.Base.getById(attribs.baseid)
                                .then(function(base)
                                {
                                    if(base){
                                        if (user.dataValues.id == base.UserId) {
                                            return dao.Base.delete(attribs, t);
                                        }
                                        else return util.throwError(400, util.Error.ERR_AUTHENTICATION, "la base no es d'aquest usuari")
                                    }
                                    else return util.throwError(400, util.Error.ERR_DELETE_BASE, "La base no es del usuari introduit")
                                })

                        }
                        else {
                            return util.throwError(400, util.Error.ERR_AUTHENTICATION, "hi ha algun problema amb l'usuari")
                        }
                    })
            }).then(util.jsonResponse.bind(util, res))
                .catch(util.resendError.bind(util, res))
                .done();
        },
        associarEstil: function (req, res) {

            util.checkParams(req.body, ['baseId', 'estilNom'])
            var attribs = {
                baseId: req.body.baseId,
                estilNom: req.body.estilNom,

            }
            db.sequelize.transaction(function (t) {
                return dao.Base.getById(attribs.baseId, t)
                    .then(function (base) {
                        if (base) {
                            return dao.Estil.getByName(attribs.estilNom, t)
                                .then(function (estil) {
                                    if (estil) {
                                        return base.addEstil(estil, {transaction: t});
                                    }
                                    else return util.throwError(400, util.Error.ERR_ESTIL_NOT_FOUND, "Err: no ha pogut obtindre el estil")
                                })
                        }
                        else return util.throwError(400, util.Error.ERR_BASE_NOT_FOUND, "Err: no ha pogut obtindre la base")
                    })
            }).then(util.jsonResponse.bind(util, res))
                .catch(util.resendError.bind(util, res))
                .done();
        },

        desassociarEstil: function (req, res) {
            var attribs = {
                baseId: req.body.baseID,
                estilNom: req.body.estilNom,

            }
            db.sequelize.transaction(function (t) {
                return dao.Base.getById(attribs.baseId, t)
                    .then(function (base) {
                        if (base) {
                            return dao.Estil.getByName(attribs.estilNom, t)
                                .then(function (estil) {
                                    if (estil) {
                                        return base.removeEstil(estil);
                                    }
                                    else return util.sendError(400, util.Error.ERR_ESTIL_NOT_FOUND, "Err: no ha pogut obtindre el estil")
                                })
                        }
                        else return util.sendError(400, util.Error.ERR_BASE_NOT_FOUND, "Err: no ha pogut obtindre la base")
                    })
            }).then(util.jsonResponse.bind(util, res))
                .catch(util.resendError.bind(util, res))
                .done();
        },
        mostraEstilsBsae:function(req,res){
          var baseid = req.params.baseid;
            if(baseid != null)
            {
                db.sequelize.transaction(function (t){
                    return dao.Base.getById(baseid,t)
                        .then(function(base){
                            if(base)
                            {
                                return base.getEstils();
                            }
                            else util.throwError(400, util.Error.ERR_BASE_NOT_FOUND, "Err: no ha pogut obtindre la base")
                        })
                }).then(util.jsonResponse.bind(util, res))
                    .catch(util.resendError.bind(util, res))
                    .done();
            }
            else
            {
                util.resendError.bind(util,res)
            }
      }  
    }
}
