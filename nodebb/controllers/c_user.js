/**
 * New node file
 */


/*
 * GET users listing.
 */

module.exports = function (app) {

  var db = app.db;
  var secret = app.secret;
  var P = app.Promise;

  var util = require('../util');
  var dao = require('../dao')(app);
  var bcrypt = require('bcrypt-nodejs');
  var jwt = require('jsonwebtoken');

  var Suser;

  return {
    login: function (req, res) {    // Login
      Suser = req.session.user;
      if(Suser){    //Si ja estem loguejats dins la session
          var token = jwt.sign({username: Suser.username}, secret);
          util.jsonResponse(res, {jwt: token, username: Suser.username});
      }
      else {    //Altrament fem el login normal
          util.checkParams(req.body, ['username', 'password']);

          dao.User.checkPassword(req.body.username, req.body.password)
              .then(function (user) {
                  var token = jwt.sign({username: user.username}, secret);
                  req.session.user = user;
                  util.jsonResponse(res, {jwt: token, username: user.username});
              })
              .catch(util.resendError.bind(util, res))
              .done();
      }
    },

    create: function (req, res) {   // New User
      util.checkParams(req.body, ['email', 'username', 'truename', 'password']);

      var attribs = {
        username: req.body.username,
        name: req.body.truename,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password),
        likes : 0
      };

      db.sequelize.transaction(function (t) {
        return P.all([
          dao.User.getByEmail(req.body.email, t),
          dao.User.getByUsername(req.body.username, t)
        ])
          .spread(function (s1, s2) {
            if (!s1 && !s2) {
              return dao.User.create(attribs, t);
            } else if (s1) {
              util.throwError(400, util.Error.ERR_ENTITY_NOT_FOUND, "Already exist a User with email = " + req.body.email);
            } else {
              util.throwError(400, util.Error.ERR_ENTITY_EXISTS, "Already exist a User with username = " + req.body.username);
            }
          })
      }).then(util.jsonResponse.bind(util, res))
        .catch(util.resendError.bind(util, res))
        .done();
    },
    
    profile: function (req, res) {  // Get Perfil
      Suser = req.session.user;
      if(Suser) {
          util.jsonResponse(res, {
              username: Suser.username,
              email: Suser.email,
              likes: Suser.likes,
              name: Suser.name
          });
      } else {
          util.throwError(400, util.Error.ERR_ENTITY_NOT_FOUND, "Not logged in. Authentication is required!");
      }
    },

    check: function (req, res) {    // Comprobar si estavem loguejats
        Suser = req.session.user;
        if(Suser){    //Si ja estem loguejats dins la session
            var token = jwt.sign({username: Suser.username}, secret);
            util.jsonResponse(res, {jwt: token, username: Suser.username});
        } else {    //Altrament no donem les dades
            util.jsonResponse(res, {msg: "Not logged in!"});
        }
    },

      logout: function (req, res) {    // Logout del usuari actual
          if (req.session.user) {    //Fem logout si estem loguejats
              req.session.user = false;
              util.jsonResponse(res,{msg : "Logout Successful!"})
          } else {    //Altrament error
              util.sendError(400, util.Error.ERR_ENTITY_NOT_FOUND, "Not logged in. Authentication is required!");
          }
      },
      mostrarUserBases:function(req,res) {
          var username = req.params.username;
          if(username != null)
          {
              db.sequelize.transaction(function (t){
                  return dao.User.getByUsername(username,t)
                      .then(function(user){
                          if(user)
                          {
                              return dao.Base.getAllBasesFromUser(user.dataValues.id, t)
                          }
                          else util.sendError(400, util.Error.ERR_USER_NOT_FOUND, "Err: no ha pogut obtindre l'usuari")
                      })
              }).then(util.jsonResponse.bind(util, res))
                  .catch(util.resendError.bind(util, res))
                  .done();
          }
          else
          {
              util.sendError(400, util.Error.ERR_USER_NOT_FOUND, "Err: el servidor no ha rebut el username")
              util.resendError.bind(util,res)
          }
      }
  }
}
