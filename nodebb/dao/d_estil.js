module.exports = function (app, dao) {
  var util = require('../util');
  var db = app.db;
  var P = app.Promise;
  var Estil = {};

  Estil.getById = function (id, t) {
    return db.Estil.find(util.addTrans(t, {where: {id: id}}));
  }

  Estil.create = function (estil_data, t) {
    return db.Estil.create(estil_data,util.addTrans(t, {}));
  }

  Estil.getByName = function (name,t){
    return db.Estil.find(util.addTrans(t,{where: {titol:name}}))
  }
  Estil.delete =function(estilId,t){
    return db.Estil.destroy(util.addTrans(t,{where:{id:estilId}}));
  }
  Estil.mostraEstils = function (options,t){
    var opt = options || {};
    //es podran considerar diversos parametres amb la variable opt
    return db.Estil.findAll(util.addTrans(t))

  }
  return Estil;
}
