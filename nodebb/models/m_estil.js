module.exports = function(sequelize, DataTypes) {
	var estil = sequelize.define('Estil', {
		description : DataTypes.STRING(1024),
		titol: DataTypes.STRING(100)
	}, {
		classMethods : {
			associate : function(models) {
				var Base = models.Base;
				estil.belongsToMany(Base,{through:'BaseEstil'});
			}
		}
	});
	return estil;
};