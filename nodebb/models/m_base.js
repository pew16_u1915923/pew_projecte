module.exports = function(sequelize, DataTypes) {
	var base = sequelize.define('Base', {
		description : DataTypes.STRING(1024),
		url: DataTypes.STRING(300),
		titol: DataTypes.STRING(100)
	},
		{
			
		classMethods : {
			associate : function(models) {
				var Estil = models.Estil;
				base.belongsToMany(Estil,{through: 'BaseEstil'});
				base.belongsTo(models.User);
			}
		}
	});

	return base;
};